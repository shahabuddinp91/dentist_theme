<?php

//its for included file
//function file_included() {
//    wp_register_style('default', get_template_directory_uri() . '/css/default/default.css', array(), '1.0', 'all');
//    wp_register_style('light', get_template_directory_uri() . '/css/light/light.css', array(), '1.0', 'all');
//    wp_register_style('dark', get_template_directory_uri() . '/css/light/light.css', array(), '1.0', 'all');
//    wp_register_style('bar', get_template_directory_uri() . '/css/bar/bar.css', array(), '1.0', 'all');
//    wp_register_style('nivo', get_template_directory_uri() . '/css/nivo-slider.css', array(), '1.0', 'all');
//    wp_register_style('main', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
//    
//    wp_enqueue_style('default');
//    wp_enqueue_style('light');
//    wp_enqueue_style('dark');
//    wp_enqueue_style('bar');
//    wp_enqueue_style('nivo');
//    wp_enqueue_style('main');
//    
//    //its for javascript link
//    wp_enqueue_script('main-js', get_template_directory_uri() . '/js/jquery-1.7.1.min.js', array(jquery), '1.3', 'true');
//    wp_enqueue_script('second-js', get_template_directory_uri() . '/js/jquery.nivo.slider.pack.js', array(jquery), '1.3', 'true');
//    
//    wp_enqueue_script('jquery');
//}
//add_action('wp_enqueue_scripts', 'file_included');
//
//function comment_scripts() {
//    if (is_singular())
//        wp_enqueue_script('comment-reply');
//}
//add_action('wp_enqueue_scripts', 'comment_scripts');

function widgets_register() {
    register_sidebar(array(
        'name' => 'Sidebar One',
        'id' => 'side_one',
        'description' => 'Use Sidebar One............',
        'before_widget' => '<div class="single_sidebar">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));
}
add_action('init', 'widgets_register');

//this code feature images support
add_theme_support('post-formats',array('aside','audio','video'));
add_theme_support('post-thumbnails');
//its for image croping
add_image_size('slide-image', 1024, 360, true);
add_image_size('post-image', 200, 200, true);
add_image_size('single-post-image', 550, 300, true);

