<div class="fix footer">
                <p>&cop&copy; SP Foundation, All Rights Reserved</p>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript">
            $(window).load(function () {
                $('#slider').nivoSlider();
            });
        </script>
        <?php wp_footer();?>
    </body>
</html>
