<?php get_header(); ?>

<?php get_template_part('slider'); ?>
<div class="fix maincontent">
    <?php get_sidebar(); ?>
    <div class="fix content">
       <?php get_template_part('post-loop'); ?>
    </div>
</div>
<?php get_footer(); ?>