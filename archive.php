<?php get_header(); ?>

<?php // get_template_part('slider'); ?>
<div class="fix maincontent">
    <?php get_sidebar(); ?>
    <div class="fix content">
        <h1 class="archivetitle">
            <?php (have_posts());?>
            <?php $post = $posts[0]; //Hac. set $post so that the_date()works.?>
            <?php if(is_category()) { ?>
            <?php _e('Archive for the');?> '<?php echo single_cat_title();?>'<?php _e('Category');?>
            <?php } elseif(is_tag()){?>
            <?php _e('Archive for the');?><?php single_tag_title();?> Tag 
            <?php } elseif(is_day()){?>
            <?php _e('Archive for');?> <?php the_time('F JS, Y');?> 
            <?php } elseif(is_month()){?> 
            <?php _e('Archive for');?><?php the_time('F, Y');?>
            <?php }elseif(is_month()){?> 
            <?php _e('Archive for');?><?php the_tiem('F,Y');?> 
            <?php }elseif(is_year()){?> 
            <?php _e('Archive for');?><?php the_time('Y');?> 
            <?php }elseif(is_search()){?> 
            <?php _e('Search Results');?> 
            <?php }elseif(is_author()){?> 
            <?php _e('Author Archive');?> 
            <?php }elseif(isset($_GET['paged']) && !empty ($_GET['paged'])){?>
            <?php _e('Blog Archives');?> 
            <?php }?> 
        </h1>
        <?php get_template_part('post-loop'); ?>
       
    </div>
</div>
<?php get_footer(); ?>