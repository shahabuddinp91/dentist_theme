<html>
    <head>
        <title><?php bloginfo('name'); ?></title>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/default/default.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/light/light.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/dark/dark.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bar/bar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/nivo-slider.css" type="text/css" media="screen" />
        
        
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
        
        
        <?php wp_head();?>
    </head>
    <body>
    <div class="fix main">
        <div class="fix hedader">
            <a href="<?php bloginfo('home'); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg">
            </a>
        </div>