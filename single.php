<?php get_header(); ?>
<div class="fix maincontent">
    <?php get_sidebar(); ?>
    <div class="fix content">
        <?php if(have_posts()) : ?><?php while (have_posts()) : the_post(); ?>
        <h2><?php the_title();?></h2>
       <div class="post_content">
                <a href="<?php the_permalink(); ?>">
                    <?php
                    the_post_thumbnail('single-post-image', array(
                        'class' => 'post-thumb',
                    ));
                    ?>
                </a>
                <?php the_content(); ?>
            </div>
        <?php comments_template('', TRUE); ?>
        <?php endwhile; ?>
        <?php else :?> 
        <h3><?php _e('404 Error&#58; Not Found'); ?></h3>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>