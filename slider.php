<div class="fix slider">
    <div class="slider-wrapper theme-light">
        <div class="nivoSlider" id="slider">
            <img src="<?php echo get_template_directory_uri(); ?>/images/nemo.jpg" alt="" title="#htmlcaption">
            <img src="<?php echo get_template_directory_uri(); ?>/images/toystory.jpg" alt="" title="#htmlcaption">
            <img src="<?php echo get_template_directory_uri(); ?>/images/up.jpg" alt="" title="#htmlcaption">
            <img src="<?php echo get_template_directory_uri(); ?>/images/walle.jpg" alt="" title="#htmlcaption">
        </div>
        <div id="htmlcaption" class="nivo-html-caption">
            <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="spfoundationbd.com">a link</a>. 
        </div>
    </div>
</div>